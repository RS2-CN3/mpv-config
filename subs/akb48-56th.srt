WEBVTT
Kind: captions
Language: en

00:00:17.500 --> 00:00:22.840
Though the ocean season has ended

00:00:23.240 --> 00:00:28.980
I could not invite you once

00:00:29.180 --> 00:00:34.520
I heard that you were busy

00:00:34.820 --> 00:00:40.280
and I hesitated

00:00:40.620 --> 00:00:45.540
My mobile phone still has a photo

00:00:45.760 --> 00:00:51.800
of you and me smiling in swimsuits last year

00:00:51.980 --> 00:00:57.080
Because when love becomes exciting

00:00:57.500 --> 00:01:02.480
you may not notice

00:01:03.220 --> 00:01:06.060
Since then (since then)

00:01:06.060 --> 00:01:08.540
Just once (just once)

00:01:08.620 --> 00:01:15.060
At that time, wasn't it sad?

00:01:15.320 --> 00:01:20.780
I will never change

00:01:20.860 --> 00:01:26.060
No matter what happens

00:01:26.060 --> 00:01:28.680
Since then (since then)

00:01:28.940 --> 00:01:31.640
We haven't met (we haven't met)

00:01:31.640 --> 00:01:38.160
And as I am,  I can't forget you

00:01:38.160 --> 00:01:43.600
That expresses my true feelings

00:01:43.780 --> 00:01:48.900
So let me keep loving you

00:01:48.960 --> 00:01:54.640
I need a reason to see you again

00:01:54.640 --> 00:02:00.480
because I feel so lonely

00:02:00.620 --> 00:02:06.100
Although I think too much

00:02:06.140 --> 00:02:11.240
have we laughed about it?

00:02:11.760 --> 00:02:14.620
I want to love you (I want to love you)

00:02:14.780 --> 00:02:17.300
To be by your side (To be by your side)

00:02:17.320 --> 00:02:23.540
Because my feelings for you will last forever

00:02:23.540 --> 00:02:29.320
A lot of time has passed

00:02:29.320 --> 00:02:34.320
and the sun doesn't shine anymore

00:02:34.320 --> 00:02:37.500
I want to love you (I want to love you)

00:02:37.540 --> 00:02:40.320
We will meet again (we will meet again)

00:02:40.460 --> 00:02:46.240
I don't want to forget another day

00:02:46.280 --> 00:02:52.240
Of all of the things I could do right now

00:02:52.240 --> 00:02:57.260
I want to stay in love with you

00:02:57.680 --> 00:03:03.020
We can't look at ourselves

00:03:03.700 --> 00:03:08.280
In an objective way

00:03:09.120 --> 00:03:14.900
But that doesn't matter in any way

00:03:15.200 --> 00:03:16.760
Once again,

00:03:16.760 --> 00:03:18.080
once again,

00:03:18.400 --> 00:03:20.400
I want to ask you out

00:03:20.580 --> 00:03:23.020
Since then (since then)

00:03:23.160 --> 00:03:26.040
Just once (just once)

00:03:26.040 --> 00:03:32.040
At that time, wasn't it sad?

00:03:32.060 --> 00:03:37.900
I will never change

00:03:38.000 --> 00:03:43.020
No matter what happens

00:03:43.020 --> 00:03:45.640
Since then (since then)

00:03:45.640 --> 00:03:48.820
We haven't met (we haven't met)

00:03:48.820 --> 00:03:54.960
And as I am, I'll never forget you

00:03:55.020 --> 00:04:00.580
That expresses my true feelings

00:04:00.720 --> 00:04:05.900
So let me keep loving you

00:04:05.920 --> 00:04:08.920
I want to love you

00:04:17.580 --> 00:04:21.020
From now on, forever

